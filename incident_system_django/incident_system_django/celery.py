import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'incident_system_django.settings')

app = Celery('incident_system_django')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


app.conf.beat_schedule = {
    'scheduled_mail_task': {
        'task': 'send_scheduled_mail_task',
        'schedule': 10.0,
    },
}
