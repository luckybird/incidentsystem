from django.urls import path, include

from rest_framework import routers

# from .view import reports_sla
from .views.reports_sla import (SlaComponentView,
                                SlaComponentDetailView,
                                SlaComponentQuartView,
                                SlaServiceView,
                                SlaServiceQuartView)

from .views.reports import (ReportSystemView,
                            ReportServiceView)

from .views.reports_problem import (ProblemView,
                                    ProblemDetailView)

from .views.job_calendar import (JobCalendarView,
                                 JobCalendarDetailView)

from .views.reports_notification import (NotificationCalendarView,
                                         NotificationCalendarDetailView,
                                         NotificationSendedView,
                                         NotificationSendedDetailView)

from .views.incident import (IncidentSimpleView,
                             IncidentDetailView,
                             IncidentFilterComponentOptionsListView,
                             IncidentFilterCategoryOptionsListView,
                             IncidentFilterListView,
                             IndexView)

from .views.answer import AnswerView
from .views.notification import NotificationCreateView
from .views.direction import DirectionCatalogView
from .views.system import SystemCatalogView
from .views.about import AboutView

from .views.export import ExportSystemsView
router = routers.DefaultRouter()

urlpatterns = [
    path('', IndexView.as_view()),
    path("<int:pk>", IncidentDetailView.as_view(), name='incident_view'),
    path("incident_simple/", IncidentSimpleView.as_view()),
    path("about/", AboutView.as_view(), name='about_url'),
    path("service_catalog/", DirectionCatalogView.as_view()),
    path("service_catalog/<int:pk>/", DirectionCatalogView.as_view()),
    path("system_catalog/", SystemCatalogView.as_view()),
    path("system_catalog/<int:pk>/", SystemCatalogView.as_view()),
    path("incident/<int:incident_id>/notification/create/",
         NotificationCreateView.as_view(),
         name='notification_create'),
    path("export_system",
         ExportSystemsView.as_view(),  name='export_systems_csv'),

    # SLA REPORTS #

    path("sla_component/",
         SlaComponentView.as_view(),
         {'external_problem': True},
         name='sla_component'),

    path("sla_component_without_external/",
         SlaComponentView.as_view(),
         {'external_problem': False},
         name='sla_component_without_external'),

    path("sla_component_detail/ \
         <int:year>/ \
         <int:quart>/ \
         <int:month>/ \
         <int:component>/ \
         <int:external_problem>",
         SlaComponentDetailView.as_view(), name='sla_component_detail'),

    path("sla_component_quart/",
         SlaComponentQuartView.as_view(), {'external_problem': True}),

    path("sla_component_quart_without_external/",
         SlaComponentQuartView.as_view(), {'external_problem': False}),

    path("sla_service/",
         SlaServiceView.as_view()),

    path("sla_service_quart/",
         SlaServiceQuartView.as_view()),

    path("reports_system/",
         ReportSystemView.as_view(),
         name='reports_system'),


    path("reports_service/",
         ReportServiceView.as_view()),

    path("answers/",
         AnswerView.as_view()),

    path("notification_calendar/",
         NotificationCalendarView.as_view()),

    path("notification_sended/",
         NotificationSendedView.as_view(),
         name='notification_sended'),

    path("notification_sended_detail/<int:pk>/",
         NotificationSendedDetailView.as_view(),
         name='notification_sended_detail'),

    path("notification_calendar/",
         NotificationCalendarView.as_view(),
         name='notification_calendar'),

    path("notification_calendar_detail/<int:pk>/",
         NotificationCalendarDetailView.as_view(),
         name='notification_calendar_detail'),

    path("job_calendar/",
         JobCalendarView.as_view(),
         name='job_calendar'),

    path("job_calendar_detail/<int:pk>",
         JobCalendarDetailView.as_view(),
         name='job_calendar_detail'),


    path("reports_problem/",
         ProblemView.as_view(),
         name='reports_problem'),

    path("reports_problem_detail/<int:pk>",
         ProblemDetailView.as_view(),
         name='reports_problem_detail'),


    # DATATABLES #

    path('api/', include(router.urls)),

    path(
        'api/filter/incidents/component/options/',
        IncidentFilterComponentOptionsListView.as_view(),
        name='incidents_filter_component_options_list'
    ),

    path(
        'api/filter/incidents/category/options/',
        IncidentFilterCategoryOptionsListView.as_view(),
        name='incidents_filter_category_options_list'
    ),

    path(
        'api/filter/incidents/',
        IncidentFilterListView.as_view(),
        name='incidents_filter_list'
    ),
]
