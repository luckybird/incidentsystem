from django.shortcuts import render, get_object_or_404
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from ..models.incident import Incident
from ..models.notification import NotificationType, NotificationQueue
from ..forms import NotificationForm
from ..tasks import send_mail_task
import datetime


class NotificationCreateView(LoginRequiredMixin, View):
    def get(self, request, incident_id):
        incident = get_object_or_404(Incident, id=incident_id)
        notificationt_type = get_object_or_404(NotificationType,
                                               id=request.GET.get("notification"))
        start_date = incident.start_date.astimezone().strftime('%d.%m.%Y')
        start_time = incident.start_date.astimezone().strftime('%H:%M')
        end_date = incident.end_date.astimezone().strftime('%d.%m.%Y')
        end_time = incident.end_date.astimezone().strftime('%H:%M')

        text = notificationt_type.text.replace('{{start_date}}', start_date) \
                                      .replace('{{start_time}}', start_time) \
                                      .replace('{{end_date}}', end_date) \
                                      .replace('{{end_time}}', end_time)

        form = NotificationForm(initial={
                                'name': notificationt_type.name,
                                'subject': notificationt_type.subject,
                                'text': text,
                                'emails': notificationt_type.emails,
                                'emails_copy': notificationt_type.emails_copy,
                                'emails_hidden': notificationt_type.emails_hidden,
                                'notification_type': notificationt_type.id,
                                'incident': incident.id,
                                'send_plan_on_calendar': notificationt_type.send_plan_on_calendar,
                                })

        return render(request, 'incident/notification.html',  {'form': form})

    def post(self, request, incident_id):
        # logger = logging.getLogger(__name__)
        post_values = request.POST.copy()
        if request.FILES:
            post_values['file_name'] = request.FILES['file'].name
        bound_form = NotificationForm(post_values, request.FILES)

        if bound_form.is_valid():
            notification = bound_form.save()
            emails_subject = bound_form.cleaned_data['subject']
            emails = tuple(((bound_form.cleaned_data['emails'].replace('\r', ''))
                           .replace('\n', ''))
                           .replace(" ", "")
                           .rstrip(';')
                           .split(';'))

            emails_copy = tuple(((bound_form.cleaned_data['emails_copy'].replace('\r', ''))
                                .replace('\n', ''))
                                .replace(" ", "")
                                .rstrip(';')
                                .split(';'))

            emails_hidden = tuple(((bound_form.cleaned_data['emails_hidden'].replace('\r', ''))
                                  .replace('\n', ''))
                                  .replace(" ", "")
                                  .rstrip(';')
                                  .split(';'))

            text_content = bound_form.cleaned_data['text']
#            import pdb; pdb.set_trace()
            match request.POST['send_select']:
                case "1":
                    send_mail_task.delay(emails_subject,
                                         emails, emails_copy,
                                         emails_hidden,
                                         text_content, notification.id)

                case "2":
                    dateandtime = bound_form.cleaned_data['send_plan_on_calendar']
                    res = []
                    incident = Incident.objects.get(pk=str(bound_form.cleaned_data['incident']))
                    for x in dateandtime.split(','):
                        dt, tm = x.split('(')
                        dt = int(dt)
                        tm = tm.split(')')[0]
                        end_date = incident.start_date - datetime.timedelta(days=dt * -1)
                        end_date_txt = end_date.strftime('%d.%m.%Y')
                        res.append(
                            datetime.datetime.strptime(f'{end_date_txt} {tm}', '%d.%m.%Y %H:%M'))
                    for dt in res:
                        NotificationQueue.objects.create(
                            name=bound_form.cleaned_data['name'],
                            subject=bound_form.cleaned_data['subject'],
                            text=text_content,
                            emails=emails,
                            emails_copy=emails_copy,
                            emails_hidden=emails_hidden,
                            incident=bound_form.cleaned_data['incident'],
                            notification_type=bound_form.cleaned_data['notification_type'],
                            send_at=dt,
                            file=request.FILES['file'] if request.FILES else None,
                            file_name=post_values['file_name'] if request.FILES else None,
                        )

                case "3":
                    dateandtime = request.POST['datetimepicker1Input']
                    res = []
                    for dt in dateandtime.strip(';').split(';'):
                        res.append(dt.lstrip())
                    for dt in res:
                        NotificationQueue.objects.create(
                            name=bound_form.cleaned_data['name'],
                            subject=bound_form.cleaned_data['subject'],
                            text=text_content,
                            emails=emails,
                            emails_copy=emails_copy,
                            emails_hidden=emails_hidden,
                            incident=bound_form.cleaned_data['incident'],
                            notification_type=bound_form.cleaned_data['notification_type'],
                            send_at=datetime.datetime.strptime(dt, '%d.%m.%Y %H:%M'),
                            file=request.FILES['file'] if request.FILES else None,
                            file_name=post_values['file_name'] if request.FILES else None,
                        )

            return redirect(reverse('incident_view', args=[incident_id]))

        return render(request, 'incident/notification.html',  {'form': bound_form})
