from django.shortcuts import render, get_object_or_404

from django.views.generic.base import View

from ..models.incident import Incident

from django.contrib.auth.mixins import LoginRequiredMixin

import json
from django.utils.safestring import SafeString

from django.db.models import Value, CharField
from datetime import datetime


class JobCalendarView(LoginRequiredMixin, View):
    def get(self, request):
        current_date = datetime.now()
        incidents = Incident.objects. \
            select_related('component__name', 'category__name'). \
            annotate(formatted_date=Value('None', output_field=CharField())). \
            filter(start_date__gte=current_date). \
            values('id', 'formatted_date', 'component__name',  'category__name', 'start_date'). \
            order_by('-start_date')
        for incident in incidents:
            start_date = incident['start_date'].astimezone().strftime('%d.%m.%Y %H:%M')
            incident['formatted_date'] = start_date
        incidents_json = json.dumps(list(incidents), indent=4, sort_keys=True, default=str)

        return render(request,
                      "job_calendar/job_calendar.html",
                      {"incidents": SafeString(incidents_json), 'nbar': 'job_calendar'})


class JobCalendarDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        incident = get_object_or_404(Incident,
                                     pk=pk)
        return render(request,
                      "job_calendar/job_calendar_detail.html",
                      {"incident": incident, 'nbar': 'job_calendar'})
