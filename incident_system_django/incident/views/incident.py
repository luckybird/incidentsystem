from django.shortcuts import render, get_object_or_404

# datables
from django.db.models import Q

from django_filters import widgets, fields, filters

from rest_framework import viewsets, status
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView


from rest_framework_datatables.django_filters.filters import GlobalFilter
from rest_framework_datatables.django_filters.filterset import DatatablesFilterSet
from rest_framework_datatables.django_filters.backends import DatatablesFilterBackend

from django.views.generic.base import View

from ..models.incident import Component, Incident, Category

from ..serializers import IncidentSerializer

from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import HttpResponse
import csv


class ExportSystems(LoginRequiredMixin, View):
    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="systems.csv"'

        writer = csv.writer(response)
        writer.writerow(['system_name'])

        systems = Component.objects.all().values_list('name')
        for system in systems:
            writer.writerow(system)

        return response


class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'incident/index.html', {'nbar': 'index'})


class IncidentSimpleView(LoginRequiredMixin, View):
    def get(self, request):
        incident = Incident.objects.all()
        return render(request,
                      "incident/incident_simple.html",
                      {"incident_list": incident, 'nbar': 'index'})


class IncidentDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        incident = get_object_or_404(Incident, id=pk)
        return render(request,
                      "incident/incident_detail.html",
                      {"incident": incident, 'nbar': 'index'})


def get_incident_options():
    return "options", {
        "component": [{'label': obj.name, 'value': obj.pk} for obj in Component.objects.all()],
        "category": [{'label': obj.name, 'value': obj.pk} for obj in Category.objects.all()]
    }


class IncidentViewSet(viewsets.ModelViewSet):
    # import pdb; pdb.set_trace()
    queryset = Incident.objects.all().order_by('id')
    serializer_class = IncidentSerializer

    def get_options(self):
        return get_incident_options()

    class Meta:
        datatables_extra_json = ('get_options',)


class YADCFMultipleChoiceWidget(widgets.QueryArrayWidget):
    def value_from_datadict(self, data, files, name):
        if name not in data:
            return None
        vals = data[name].split("|")
        new_data = data.copy()
        new_data[name] = vals
        return super().value_from_datadict(new_data, files, name)


class YADCFModelMultipleChoiceField(fields.ModelMultipleChoiceField):
    widget = YADCFMultipleChoiceWidget


class YADCFModelMultipleChoiceFilter(filters.ModelMultipleChoiceFilter):
    field_class = YADCFModelMultipleChoiceField

    def global_q(self):
        """
        This method is necessary for the global filter
        - i.e. any string values entered into the search box.
        """
        if not self._global_search_value:
            return Q()
        kw = "{}__{}".format(self.field_name, self.lookup_expr)
        return Q(**{kw: self._global_search_value})


class GlobalCharFilter(GlobalFilter, filters.CharFilter):
    pass


class GlobalNumberFilter(GlobalFilter, filters.NumberFilter):
    pass


class GlobalDateTimeFilter(GlobalFilter, filters.DateTimeFilter):
    pass


class IncidentFilter(DatatablesFilterSet):
    component_name = YADCFModelMultipleChoiceFilter(
        field_name="component__name", queryset=Component.objects.all(), lookup_expr="contains"
    )

    category_name = YADCFModelMultipleChoiceFilter(
        field_name="category__name", queryset=Category.objects.all(), lookup_expr="contains"
    )

    start_date = filters.IsoDateTimeFilter(field_name="start_date", lookup_expr='gte')
    end_date = filters.IsoDateTimeFilter(field_name="end_date", lookup_expr='lte')

    id = GlobalNumberFilter()
    name = GlobalCharFilter()

    class Meta:
        model = Incident
        fields = ("id",
                  "start_date",
                  "end_date",
                  "component_name",
                  "category_name",
                  "external_problem")


class IncidentFilterListView(generics.ListAPIView):
    queryset = Incident.objects.all().select_related("component", "category").order_by('start_date')
    serializer_class = IncidentSerializer
    filter_backends = (DatatablesFilterBackend,)
    filterset_class = IncidentFilter


class IncidentFilterComponentOptionsListView(APIView):
    allowed_methods = ("GET",)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        components = list(
            Component.objects.filter()
            .values_list("id", "name")
            .order_by("name")
            .distinct()
        )
        options = list()
        for id_, name in components:
            options.append({"value": str(id_), "label": name})
        return Response(data={"options": options}, status=status.HTTP_200_OK)


class IncidentFilterCategoryOptionsListView(APIView):
    allowed_methods = ("GET",)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        category = list(
            Category.objects.filter()
            .values_list("id", "name")
            .order_by("name")
            .distinct()
        )
        options = list()
        for id_, name in category:
            options.append({"value": str(id_), "label": name})
        return Response(data={"options": options}, status=status.HTTP_200_OK)
