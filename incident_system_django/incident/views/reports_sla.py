from django.shortcuts import render

from django.views.generic.base import View


from ..models.incident import Component,  Incident, Service


from django.contrib.auth.mixins import LoginRequiredMixin

from ..forms import YearForm

from django.db.models import Q

from datetime import datetime

import calendar


import decimal
# Create your views here.


class SlaComponentView(LoginRequiredMixin, View):
    def get(self, request, external_problem, year=None):  # noqa
        decimal.getcontext().prec = 6
        ms = {}  # mounth sla

        if request.GET.get('year', '').isdigit():
            year = int(request.GET.get('year'))
        else:
            year = datetime.now().year

        current_year = datetime.now().year
        current_month = datetime.now().month
        form = YearForm(request.GET or None)

        for month in range(1, 13):
            need_set_data = False if month > current_month and year >= current_year else True
            last_month_day = calendar.monthrange(year, month)
            month_minutes = last_month_day[1] * 24 * 60

            if need_set_data:
                # import pdb; pdb.set_trace()
                month_start = f"{year}-{month}-1T00:00Z"
                month_end = f"{year}-{month}-{last_month_day[1]}T23:59Z"
                all_month_inc = Incident.objects.select_related(
                    "component",
                    "category") \
                    .only(
                    "start_date",
                    "end_date",
                    "category",
                    "external_problem",
                    "component__name") \
                    .filter(
                    Q
                        (Q(start_date__gte=month_start) &
                            Q(start_date__lte=month_end))
                    | Q
                        (Q(start_date__lt=month_start) &
                            Q(end_date__gte=month_start) &
                            Q(end_date__lte=month_end)),
                    category__sla=True
                )

                if not external_problem:
                    all_month_inc = all_month_inc.filter(
                        external_problem=False)

                for inc in all_month_inc:
                    if inc.end_date > datetime.strptime(month_end, '%Y-%m-%dT%H:%M%z'):
                        inc.end_date = datetime.strptime(
                            month_end, '%Y-%m-%dT%H:%M%z')
                    if inc.start_date < datetime.strptime(month_start, '%Y-%m-%dT%H:%M%z'):
                        inc.start_date = datetime.strptime(
                            month_start, '%Y-%m-%dT%H:%M%z')

            for component in Component.objects.all():
                if component.name not in ms:
                    ms[component.name] = {}
                if need_set_data:
                    month_unavailability = 0
                    number_of_month_incident = 0
                    for inc in all_month_inc:
                        if inc.component == component:
                            month_unavailability = month_unavailability + (
                                (inc.end_date - inc.start_date).total_seconds() / 60)
                            number_of_month_incident += 1
                    month_unavailability = round(month_unavailability)
                    sla = decimal.Decimal(
                        ((month_minutes - month_unavailability) / month_minutes)
                        * 100).quantize(decimal.Decimal("1.000"), decimal.ROUND_FLOOR)

                    if component.sla:
                        availability_max = decimal.Decimal(
                            month_minutes - month_minutes * component.sla / 100)
                        availability_15 = decimal.Decimal(
                            availability_max * 85 / 100)
                        if month_unavailability > availability_max:
                            alert_color = "bg-danger"
                        elif month_unavailability >= availability_15:
                            alert_color = "bg-warning"
                        else:
                            alert_color = "bg-success"
                    else:
                        alert_color = ""

                    ms[component.name][month] = {
                        "unavailability": month_unavailability,
                        "sla": str(sla),
                        "current_sla": component.sla,
                        "alert_color": alert_color,
                        "number_of_incident":  number_of_month_incident,
                        "component_id": component.id
                    }
                else:
                    ms[component.name][month] = {
                        "unavailability": 0,
                        "sla": "NaN",
                        "current_sla": component.sla,
                        "component_id": component.id
                    }

        return render(
            request,
            "reports/sla_component.html",
            {
                "ms": ms,
                "nbar": 'sla_component',
                "get_year": year,
                "quart": 0,
                "external_problem": 1 if external_problem else 0,
                "form": form
            }
        )


class SlaComponentQuartView(LoginRequiredMixin, View):
    def get(self, request, external_problem, year=None):  # noqa
        decimal.getcontext().prec = 6
        ms = {}  # mounth sla
        last_month_day = (31, 30, 30, 31)

        if request.GET.get('year', '').isdigit():
            year = int(request.GET.get('year'))
        else:
            year = datetime.now().year

        current_year = datetime.now().year
        current_month = datetime.now().month
        current_quart = int(f'{(current_month-1)//3+1}')

        form = YearForm(request.GET or None)

        for quart in range(1, 5):
            need_set_data = False if quart > current_quart and year >= current_year else True
            quart_start = f"{year}-{quart * 3 - 2}-01T00:00Z"
            quart_end = f"{year}-{quart * 3}-{last_month_day[quart-1]}T23:59Z"
            quart_days = datetime.strptime(
                quart_end, '%Y-%m-%dT%H:%M%z') - datetime.strptime(quart_start, '%Y-%m-%dT%H:%M%z')
            quart_minutes = quart_days.days * 24 * 60

            if need_set_data:
                all_quart_inc = Incident.objects.select_related("component", "category") \
                    .only("start_date", "end_date", "category",
                          "external_problem", "component__name") \
                    .filter(
                    Q
                        (Q(start_date__gte=quart_start) &
                            Q(start_date__lte=quart_end))
                    | Q
                        (Q(start_date__lt=quart_start) &
                            Q(end_date__gte=quart_start) &
                            Q(end_date__lte=quart_end)),
                    category__sla=True
                )
                if not external_problem:
                    all_quart_inc = all_quart_inc.filter(
                        external_problem=False)

                for inc in all_quart_inc:
                    if inc.end_date > datetime.strptime(quart_end, '%Y-%m-%dT%H:%M%z'):
                        inc.end_date = datetime.strptime(
                            quart_end, '%Y-%m-%dT%H:%M%z')
                    if inc.start_date < datetime.strptime(quart_start, '%Y-%m-%dT%H:%M%z'):
                        inc.start_date = datetime.strptime(
                            quart_start, '%Y-%m-%dT%H:%M%z')
            # import pdb; pdb.set_trace()
            for component in Component.objects.all():
                if component.name not in ms:
                    ms[component.name] = {}
                if need_set_data:
                    quart_unavailability = 0
                    number_of_quart_incident = 0
                    for inc in all_quart_inc:
                        if inc.component == component:
                            quart_unavailability = quart_unavailability + (
                                (inc.end_date - inc.start_date).total_seconds() / 60)
                            number_of_quart_incident += 1
                    quart_unavailability = round(quart_unavailability)

                    sla = decimal.Decimal(
                        (quart_minutes - quart_unavailability) / quart_minutes
                        * 100).quantize(decimal.Decimal("1.000"), decimal.ROUND_FLOOR)

                    if component.sla:
                        availability_max = decimal.Decimal(
                            quart_minutes - quart_minutes * component.sla / 100)
                        availability_15 = decimal.Decimal(
                            availability_max * 85 / 100)
                        if quart_unavailability > availability_max:
                            alert_color = "bg-danger"
                        elif quart_unavailability >= availability_15:
                            alert_color = "bg-warning"
                        else:
                            alert_color = "bg-success"
                    else:
                        alert_color = ""

                    ms[component.name][quart] = {
                        "unavailability": quart_unavailability,
                        "sla": str(sla),
                        "current_sla": component.sla,
                        "alert_color": alert_color,
                        "number_of_incident": number_of_quart_incident,
                        "component_id": component.id
                    }
                else:
                    ms[component.name][quart] = {
                        "unavailability": 0,
                        "sla": "NaN",
                        "current_sla": component.sla,
                        "component_id": component.id
                    }

        return render(
            request,
            "reports/sla_component_quart.html",
            {
                "ms": ms,
                "nbar": 'sla_component',
                "get_year": year,
                "quart": quart,
                "external_problem": 1 if external_problem else 0,
                "form": form
            }
        )


class SlaComponentDetailView(LoginRequiredMixin, View):
    def get(self, request, year, quart, month, component, external_problem):
        # import pdb; pdb.set_trace()
        ips = {}  # incidents with problem sla
        external_problem = True if external_problem == 1 else False
        component = Component.objects.get(pk=component)
        if quart == 0:
            last_month_day = calendar.monthrange(year, month)
            month_start = f"{year}-{month}-1T00:00Z"
            month_end = f"{year}-{month}-{last_month_day[1]}T23:59Z"
            incidents = Incident.objects.select_related(
                "component",
                "category") \
                .only(
                "start_date",
                "end_date",
                "category",
                "external_problem",
                "component__name") \
                .filter(
                Q
                    (Q(start_date__gte=month_start) &
                        Q(start_date__lte=month_end))
                | Q
                    (Q(start_date__lt=month_start) &
                        Q(end_date__gte=month_start) &
                        Q(end_date__lte=month_end)),
                category__sla=True,
                component=component
            )
            if not external_problem:
                incidents = incidents.filter(external_problem=False)

        else:
            last_month_day = (31, 30, 30, 31)
            quart_start = f"{year}-{quart * 3 - 2}-01T00:00Z"
            quart_end = f"{year}-{quart * 3}-{last_month_day[quart-1]}T23:59Z"
            incidents = Incident.objects.select_related(
                "component",
                "category") \
                .only(
                "start_date",
                "end_date",
                "category",
                "external_problem",
                "component__name") \
                .filter(
                Q
                    (Q(start_date__gte=quart_start) &
                        Q(start_date__lte=quart_end))
                | Q
                    (Q(start_date__lt=quart_start) &
                        Q(end_date__gte=quart_start) &
                        Q(end_date__lte=quart_end)),
                category__sla=True,
                component=component
            )
            if not external_problem:
                incidents = incidents.filter(external_problem=False)

        for incident in incidents:
            ips[incident.id] = {
                "start_date": incident.start_date,
                "end_date": incident.end_date,
            }

        return render(
            request,
            "reports/sla_component_detail.html",
            {
                "ips": ips,
                "nbar": 'sla_component',
                "component": component.name,
                "year": year,
                "month": month
            }
        )


class SlaServiceView(LoginRequiredMixin, View):
    def get(self, request, year=None):  # noqa
        decimal.getcontext().prec = 6
        ms = {}  # mounth sla
        current_year = datetime.now().year
        current_month = datetime.now().month

        if request.GET.get('year', '').isdigit():
            year = int(request.GET.get('year'))
        else:
            year = datetime.now().year

        form = YearForm(request.GET or None)

        for month in range(1, 13):
            need_set_data = False if month > current_month and year >= current_year else True
            last_month_day = calendar.monthrange(year, month)
            month_start = f"{year}-{month}-1T00:00Z"
            month_end = f"{year}-{month}-{last_month_day[1]}T23:59Z"
            month_minutes = last_month_day[1] * 24 * 60
            service_list = Service.objects.all()
            for service in service_list:
                all_month_inc = Incident.objects.select_related(
                    "component",
                    "category") \
                    .only(
                    "start_date",
                    "end_date",
                    "category",
                    "external_problem",
                    "component__name") \
                    .filter(
                    Q
                        (Q(start_date__gte=month_start) &
                            Q(start_date__lte=month_end))
                    | Q
                        (Q(start_date__lt=month_start) &
                            Q(end_date__gte=month_start) &
                            Q(end_date__lte=month_end)),
                    category__sla=True,
                    component__service=service
                )
                for inc in all_month_inc:
                    if inc.end_date > datetime.strptime(month_end, '%Y-%m-%dT%H:%M%z'):
                        inc.end_date = datetime.strptime(
                            month_end, '%Y-%m-%dT%H:%M%z')
                    if inc.start_date < datetime.strptime(month_start, '%Y-%m-%dT%H:%M%z'):
                        inc.start_date = datetime.strptime(
                            month_start, '%Y-%m-%dT%H:%M%z')

                if need_set_data:
                    month_unavailability = 0
                    number_of_month_incident = 0
                    for inc in all_month_inc:
                        month_unavailability = month_unavailability + (
                            (inc.end_date - inc.start_date).total_seconds() / 60)
                        number_of_month_incident += 1
                    unavailability = round(month_unavailability)

                if service.name not in ms:
                    ms[service.name] = {}

                if need_set_data:
                    sla = decimal.Decimal(
                        ((month_minutes - unavailability) / month_minutes) *
                        100).quantize(decimal.Decimal("1.000"), decimal.ROUND_FLOOR)

                    if service.sla:
                        availability_max = decimal.Decimal(
                            month_minutes - month_minutes * service.sla / 100)
                        availability_15 = decimal.Decimal(
                            availability_max * 85 / 100)
                        if unavailability > availability_max:
                            alert_color = "bg-danger"
                        elif unavailability >= availability_15:
                            alert_color = "bg-warning"
                        else:
                            alert_color = "bg-success"
                    else:
                        alert_color = ""

                    ms[service.name][month] = {"unavailability": unavailability,
                                               "sla": str(sla),
                                               "current_sla": service.sla,
                                               "alert_color": alert_color,
                                               "number_of_incident": number_of_month_incident}
                else:
                    ms[service.name][month] = {"unavailability": 0,
                                               "sla": "NaN",
                                               "current_sla": service.sla}

        return render(
            request,
            "reports/sla_service.html",
            {
                "ms": ms,
                "nbar": 'sla_service',
                "get_year": year,
                "form": form
            }
        )


class SlaServiceQuartView(LoginRequiredMixin, View):
    def get(self, request, year=None):  # noqa
        """ Функция по расчёту выполнения sla за квартал """
        decimal.getcontext().prec = 6
        ms = {}  # mounth sla

        if request.GET.get('year', '').isdigit():
            year = int(request.GET.get('year'))
        else:
            year = datetime.now().year

        current_year = datetime.now().year
        current_month = datetime.now().month
        current_quart = int(f'{(current_month-1)//3+1}')

        form = YearForm(request.GET or None)

        last_month_day = (31, 30, 30, 31)
        for quart in range(1, 5):
            need_set_data = False if quart > current_quart and year >= current_year else True
            quart_start = f"{year}-{quart * 3 - 2}-01T00:00Z"
            quart_end = f"{year}-{quart * 3}-{last_month_day[quart-1]}T23:59Z"
            quart_days = datetime.strptime(
                quart_end, '%Y-%m-%dT%H:%M%z') - datetime.strptime(quart_start, '%Y-%m-%dT%H:%M%z')
            quart_minutes = quart_days.days * 24 * 60
            service_list = Service.objects.all()
            for service in service_list:
                all_quart_inc = Incident.objects.select_related(
                    "component",
                    "category") \
                    .only(
                    "start_date",
                    "end_date",
                    "category",
                    "external_problem",
                    "component__name") \
                    .filter(
                    Q
                        (Q(start_date__gte=quart_start) &
                            Q(start_date__lte=quart_end))
                    | Q
                        (Q(start_date__lt=quart_start) &
                            Q(end_date__gte=quart_start) &
                            Q(end_date__lte=quart_end)),
                    category__sla=True,
                    component__service=service
                )

                for inc in all_quart_inc:
                    if inc.end_date > datetime.strptime(quart_end, '%Y-%m-%dT%H:%M%z'):
                        inc.end_date = datetime.strptime(
                            quart_end, '%Y-%m-%dT%H:%M%z')
                    if inc.start_date < datetime.strptime(quart_start, '%Y-%m-%dT%H:%M%z'):
                        inc.start_date = datetime.strptime(
                            quart_start, '%Y-%m-%dT%H:%M%z')

                if need_set_data:
                    quart_unavailability = 0
                    number_of_quart_incident = 0
                    for inc in all_quart_inc:
                        quart_unavailability = quart_unavailability + (
                            (inc.end_date - inc.start_date).total_seconds() / 60)
                        number_of_quart_incident += 1
                    unavailability = round(quart_unavailability)

                if service.name not in ms:
                    ms[service.name] = {}

                if need_set_data:
                    sla = decimal.Decimal(
                        (quart_minutes - unavailability) / quart_minutes
                        * 100).quantize(decimal.Decimal("1.000"), decimal.ROUND_FLOOR)
                    if service.sla:
                        availability_max = decimal.Decimal(
                            quart_minutes - quart_minutes * service.sla / 100)
                        availability_15 = decimal.Decimal(
                            availability_max * 85 / 100)
                        if unavailability > availability_max:
                            alert_color = "bg-danger"
                        elif quart_unavailability >= availability_15:
                            alert_color = "bg-warning"
                        else:
                            alert_color = "bg-success"
                    else:
                        alert_color = ""

                    ms[service.name][quart] = {"unavailability": unavailability,
                                               "sla": str(sla),
                                               "current_sla": service.sla,
                                               "alert_color": alert_color,
                                               "number_of_incident": number_of_quart_incident}

                else:
                    ms[service.name][quart] = {"unavailability": 0,
                                               "sla": "NaN",
                                               "current_sla": service.sla}

        return render(
            request,
            "reports/sla_service_quart.html",
            {
                "ms": ms,
                "nbar": 'sla_service',
                "get_year": year,
                "form": form
            }
        )
