from django.shortcuts import render, get_object_or_404

from django.views.generic.base import View

from ..models.notification import Notification, NotificationQueue

from django.contrib.auth.mixins import LoginRequiredMixin

import json
from django.utils.safestring import SafeString

from django.db.models import Value, CharField


class NotificationCalendarView(LoginRequiredMixin, View):
    def get(self, request):
        notifications = NotificationQueue.objects. \
            select_related('incident__component'). \
            annotate(formatted_date=Value('None', output_field=CharField())). \
            values('pk', 'incident__component__name', 'formatted_date', 'subject', 'send_at'). \
            order_by('-send_at')
        for notification in notifications:
            send_at = notification['send_at'].astimezone().strftime('%d.%m.%Y %H:%M')
            notification['formatted_date'] = send_at
        notifications_json = json.dumps(list(notifications), indent=4, sort_keys=True, default=str)

        return render(request,
                      "reports/notification_calendar.html",
                      {"notifications": SafeString(notifications_json), 'nbar': 'reports'})


class NotificationSendedView(LoginRequiredMixin, View):
    def get(self, request):
        notifications = Notification.objects. \
            select_related('incident__component'). \
            annotate(formatted_date=Value('None', output_field=CharField())). \
            values('pk', 'incident__component__name', 'formatted_date', 'subject', 'created_at'). \
            order_by('-created_at')
        for notification in notifications:
            created_at = notification['created_at'].astimezone().strftime('%d.%m.%Y %H:%M')
            notification['formatted_date'] = created_at
        notifications_json = json.dumps(list(notifications), indent=4, sort_keys=True, default=str)
        return render(request,
                      "reports/notification_sended.html",
                      {"notifications": SafeString(notifications_json), 'nbar': 'reports'})


class NotificationSendedDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        # import pdb; pdb.set_trace()
        notification = get_object_or_404(Notification,
                                         pk=pk)

        return render(request,
                      "reports/notification_sended_detail.html",
                      {"notification": notification, 'nbar': 'reports'})


class NotificationCalendarDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        # import pdb; pdb.set_trace()
        notification = get_object_or_404(NotificationQueue,
                                         pk=pk)

        return render(request,
                      "reports/notification_calendar_detail.html",
                      {"notification": notification, 'nbar': 'reports'})
