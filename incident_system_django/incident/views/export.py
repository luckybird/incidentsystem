from django.views.generic.base import View
from ..models.incident import Component
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
import csv


class ExportSystemsView(LoginRequiredMixin, View):
    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="systems.csv"'

        writer = csv.writer(response)
        writer.writerow(['system_name'])

        systems = Component.objects.all().values_list('name')
        for system in systems:
            writer.writerow(system)

        return response
