from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from ..models.incident import Component
from ..forms import SystemForm


class SystemCatalogView(LoginRequiredMixin, View):
    def get(self, request):
        form = SystemForm(initial={'system': 0})
        return render(request,
                      "incident/component_catalog.html",
                      {"form": form, 'nbar': 'sla_component'})

    def post(self, request):
        form = SystemForm(request.POST)
        if form.is_valid():
            system_selected = form.cleaned_data['system']
            system = Component.objects.get(id=system_selected.id)
            return render(request,
                          "incident/component_catalog.html",
                          {"form": form, "component": system,  'nbar': 'sla_component'})
        else:
            form = SystemForm(initial={'system': 0})
            return render(request,
                          "incident/component_catalog.html",
                          {"form": form, 'nbar': 'sla_component'})
