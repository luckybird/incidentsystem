from django.shortcuts import render, get_object_or_404

from django.views.generic.base import View

from ..models.incident import Problem, Incident
from django.contrib.auth.mixins import LoginRequiredMixin

import json
from django.utils.safestring import SafeString

from django.db.models import Value, Count, Sum, F

import decimal 


class ProblemView(LoginRequiredMixin, View):
    def get(self, request):
        problems = Problem.objects.select_related('problem__component').annotate(count=Count('incidents')).annotate(sum = Sum(F('incidents__end_date') - F('incidents__start_date'))).values('id', 'component__name', 'status__name', 'name','start_date', 'end_date', 'count', 'sum').order_by('-start_date')
        for problem in problems:
            start_date = problem['start_date'].astimezone().strftime('%d.%m.%Y %H:%M')
            end_date = problem['end_date'].astimezone().strftime('%d.%m.%Y %H:%M')
            problem['formatted_start_date'] = start_date
            problem['formatted_end_date'] = end_date
            if problem['sum']:
                sum_min = problem['sum'].total_seconds() / 60
                sum_min = decimal.Decimal(sum_min)
            else:
                sum_min = 0
            problem['formatted_sum_min'] = sum_min
            # import pdb; pdb.set_trace()
        problems_json = json.dumps(list(problems), indent=4, sort_keys=True, default=str)

        return render(request,
                      "reports/problem.html",
                      {"notifications": SafeString(problems_json), 'nbar': 'reports'})


class ProblemDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        # import pdb; pdb.set_trace()
        problem = Problem.objects.annotate(count=Count('incidents')).annotate(sum = Sum(F('incidents__end_date') - F('incidents__start_date'))).order_by('-start_date').get(pk=pk)
        incident = Incident.objects.filter(problem=pk).values('id', 'component__name', 'description')
        incident_json = json.dumps(list(incident), indent=4, sort_keys=True, default=str)
        # import pdb; pdb.set_trace()
        return render(request,
                      "reports/problem_detail.html",
                      {"problem": problem, "incident": SafeString(incident_json), 'nbar': 'reports'})

