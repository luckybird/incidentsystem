from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin


class AboutView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request,
                      "incident/about.html",
                      {'nbar': 'about'})
