from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from ..models.incident import Incident, Category

from ..forms import SystemYearForm

import calendar


class ReportSystemView(LoginRequiredMixin, View):
    def get(self, request):

        form_system_year = SystemYearForm(request.GET or None)
        

        return render(
            request,
            "incident/reports_system.html",
            {
                "nbar": 'reports_system',
                "form_system_year": form_system_year,
            }
        )

    def post(self, request):
        mr = {}  # mounth report

        form_system_year = SystemYearForm(request.POST or None)
        if not form_system_year.is_valid():
            form_system_year = SystemYearForm()
            return render(
                request,
                "incident/reports_system.html",
                {
                    "nbar": 'reports_system',
                    "form_system_year": form_system_year
                }
            )

        system = form_system_year.data['system']
        year = int(form_system_year.data['year'])

        if system and year:

            for month in range(1, 13):
                last_month_day = calendar.monthrange(year, month)
                month_start = f"{year}-{month}-1T00:00Z"
                month_end = f"{year}-{month}-{last_month_day[1]}T23:59Z"

                all_month_inc = Incident.objects.select_related(
                    "component",
                    "category") \
                    .only(
                    "start_date",
                    "end_date",
                        "category",
                        "external_problem",
                        "component__name") \
                    .filter(
                    start_date__gte=month_start,
                    end_date__lte=month_end,
                    component=system
                )
                for category in Category.objects.all():
                    month_unavailability = 0
                    number_of_month_incident = 0
                    if category.name not in mr:
                        mr[category.name] = {}
                    for inc in all_month_inc:
                        if inc.category == category:
                            month_unavailability = month_unavailability + (
                                (inc.end_date - inc.start_date).total_seconds() / 60)
                            number_of_month_incident += 1
                    month_unavailability = round(month_unavailability)
                    mr[category.name][month] = {
                        "number_of_incident":  number_of_month_incident,
                        "unavailability": month_unavailability,
                    }

        return render(
            request,
            "incident/reports_system.html",
            {
                "mr": mr,
                "nbar": 'reports_system',
                "get_year": year,
                "component": system,
                "form_system_year": form_system_year
            }
        )


class ReportServiceView(LoginRequiredMixin, View):
    def get(self, request, year=None):
        pass
