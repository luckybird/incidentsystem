from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin

from ..models.answer import Answer


class AnswerView(LoginRequiredMixin, View):
    def get(self, request):
        # import pdb; pdb.set_trace()
        answers = Answer.objects.all().order_by('name')
        return render(
            request,
            "incident/answer.html",
            {"answers": answers,
             'nbar': 'answer'}
        )

