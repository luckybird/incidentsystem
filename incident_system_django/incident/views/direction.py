from django.shortcuts import render
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from ..models.incident import Component
from ..forms import ServiceForm


class DirectionCatalogView(LoginRequiredMixin, View):
    def get(self, request):
        form = ServiceForm(initial={'service': 0})
        return render(request,
                      "incident/service_catalog.html",
                      {"form": form, 'nbar': 'sla_component'})

    def post(self, request):
        form = ServiceForm(request.POST)
        if form.is_valid():
            service_selected = form.data['service']
            component_list = Component.objects.all().filter(service=service_selected)
            form = ServiceForm(request.POST or None)
            return render(request,
                          "incident/service_catalog.html",
                          {"form": form, "component_list": component_list,
                           'nbar': 'sla_component'})
        else:
            form = ServiceForm(initial={'service': 0})
            return render(request,
                          "incident/service_catalog.html",
                          {"form": form, 'nbar': 'sla_component'})
