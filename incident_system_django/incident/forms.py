from django.forms import ModelForm
from .models.incident import Service, Component, Incident
from .models.notification import Notification
from django import forms
import re

import datetime
from django.db.models import Min
from django.db.models.functions import ExtractYear
# from django.core.exceptions import ValidationError


EMAIL_REGEX = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+,?(\x20)*([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+,?(\x20)*)*)"  # noqa


class NotificationForm(ModelForm):
    class Meta:
        model = Notification
        widgets = {
            'name': forms.HiddenInput(),
            'subject': forms.TextInput(attrs={'class': 'form-control'}),
            'text': forms.Textarea(attrs={'class': 'form-control'}),
            'emails': forms.Textarea(attrs={'class': 'form-control'}),
            'emails_copy': forms.Textarea(attrs={'class': 'form-control'}),
            'emails_hidden': forms.Textarea(attrs={'class': 'form-control'}),
            'file': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'send_plan_on_calendar': forms.TextInput(attrs={'class': 'form-control'}),
            'file_name': forms.HiddenInput(),
            'incident': forms.HiddenInput(),
            'notification_type': forms.HiddenInput(),
        }

        labels = {
            'incident': '',
            'notification_type': '',
            'file_name': ''
        }
        fields = [
            'name',
            'subject',
            'text',
            'emails',
            'emails_copy',
            'emails_hidden',
            'file',
            'send_plan_on_calendar',
            'file_name',
            'incident',
            'notification_type']

    def clean_emails(self):
        email_list = self.cleaned_data["emails"].lower()
        if email_list and not re.match(EMAIL_REGEX, email_list):
            raise forms.ValidationError(
                'Неправивльный список почтовых адресов!')
        return email_list

    def clean_emails_copy(self):
        email_list = self.cleaned_data["emails_copy"].lower()
        if email_list and not re.match(EMAIL_REGEX, email_list):
            raise forms.ValidationError(
                'Неправивльный список почтовых адресов!')
        return email_list

    def clean_emails_hidden(self):
        email_list = self.cleaned_data["emails_hidden"].lower()
        if email_list and not re.match(EMAIL_REGEX, email_list):
            raise forms.ValidationError(
                'Неправивльный список почтовых адресов!')
        return email_list


class ServiceForm(ModelForm):
    service = forms.ModelChoiceField(queryset=Service.objects.all(),
                                     widget=forms.Select(
                                         attrs={'class': 'form-control'}),
                                     label='Выберите сервис', label_suffix=':'
                                     )

    class Meta:
        model = Service
        fields = [
        ]


class SystemForm(ModelForm):
    system = forms.ModelChoiceField(queryset=Component.objects.all(),
                                    widget=forms.Select(
                                        attrs={'class': 'form-control'}),
                                    label='Выберите cистему', label_suffix=':'
                                    )

    class Meta:
        model = Component
        fields = [
        ]


class YearForm(forms.Form):

    year = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
                             label='Выберите год',
                             label_suffix=':',
                             required=False)
    def __init__(self, *args, **kwargs):
        super(YearForm, self).__init__(*args, **kwargs)
        start_year = int(Incident.objects.only('start_date').earliest(
            'start_date').start_date.strftime("%Y"))
        end_year = int(Incident.objects.only('end_date').latest(
            'start_date').start_date.strftime("%Y"))
        list_year = tuple(range(start_year, end_year + 1))
        choices = (("", "----"), *zip(list_year, list_year))
        self.fields['year'].choices = choices


class SystemYearForm(SystemForm, YearForm):
    pass
