# Generated by Django 3.2.9 on 2023-03-17 06:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0063_auto_20230317_0930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incident',
            name='problem',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='problems', to='incident.problem', verbose_name='Проблема'),
        ),
    ]
