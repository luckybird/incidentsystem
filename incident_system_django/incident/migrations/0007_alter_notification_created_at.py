# Generated by Django 3.2.9 on 2022-05-13 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0006_remove_component_notification_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Время отправки'),
        ),
    ]
