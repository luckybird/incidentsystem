# Generated by Django 3.2.9 on 2022-11-24 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0042_auto_20221110_0006'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationSendPlan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plan_type', models.CharField(choices=[('NOW', 'Моментальная отправка'), ('ONDATE', 'Отправка по дате'), ('CALENDAR', 'Отправка по календарю')], default=None, max_length=8, verbose_name='Тип плана рассылки')),
                ('on_date', models.DateTimeField(auto_now_add=True, verbose_name='Время отправки')),
                ('calendar', models.CharField(blank=True, max_length=100, null=True, verbose_name='Календарь дат отправо')),
                ('notification', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='send_plans', to='incident.notificationtype', verbose_name='Рассылка')),
            ],
        ),
    ]
