# Generated by Django 3.2.9 on 2023-04-10 09:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0072_alter_profile_department'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProblemStatus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=30, null=True, verbose_name='Статус проблемы')),
            ],
            options={
                'verbose_name': 'Статус проблемы',
                'verbose_name_plural': 'Статусы проблем',
            },
        ),
        migrations.AddField(
            model_name='problem',
            name='status',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='statuses', to='incident.problemstatus'),
        ),
    ]
