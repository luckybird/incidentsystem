# Generated by Django 3.2.9 on 2023-03-10 08:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0057_alter_component_service'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Наименование шаблона ответа')),
                ('text', models.TextField(blank=True, max_length=5000, null=True, verbose_name='Шаблон текста ответа')),
            ],
            options={
                'verbose_name': 'Шаблоны ответов',
                'verbose_name_plural': 'Шаблоны ответов',
            },
        ),
    ]
