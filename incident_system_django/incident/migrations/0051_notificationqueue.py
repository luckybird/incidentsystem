# Generated by Django 3.2.9 on 2023-01-17 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incident', '0050_auto_20230111_0914'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationQueue',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Назваение оповещения')),
                ('subject', models.CharField(max_length=150, verbose_name='Фактически отправленный текст темы сообщения')),
                ('text', models.TextField(blank=True, max_length=5000, null=True, verbose_name='Фактически отправленный текст сообщения')),
                ('emails', models.TextField(max_length=6000, verbose_name='Фактически отправленный перечень email адресов')),
                ('emails_copy', models.TextField(blank=True, max_length=6000, null=True, verbose_name='Шаблон переченя email адресов в копии')),
                ('emails_hidden', models.TextField(blank=True, max_length=6000, null=True, verbose_name='Шаблон переченя email адресов в скрытой копии')),
                ('file', models.FileField(blank=True, null=True, upload_to='notifications/%Y/%m/%d', verbose_name='Отправленный файл')),
                ('file_name', models.CharField(blank=True, max_length=250, null=True, verbose_name='Имя файла')),
                ('incident', models.PositiveIntegerField(verbose_name='Инцидент')),
                ('notification_type', models.PositiveSmallIntegerField(verbose_name='Тип рассылки')),
                ('send_at', models.DateTimeField(auto_now_add=True, verbose_name='Время и дата  отправки')),
            ],
            options={
                'verbose_name': 'Очередь отправки оповещений',
                'verbose_name_plural': 'Очередь отправки оповещений',
            },
        ),
    ]
