from django.db import models
# Create your models here.


class Answer(models.Model):
    name = models.CharField(max_length=150,
                            verbose_name="Наименование шаблона ответа")
    text = models.TextField(max_length=5000,
                            blank=True, null=True,
                            verbose_name="Шаблон текста ответа")

    class Meta:
        verbose_name = 'Шаблоны ответов'
        verbose_name_plural = 'Шаблоны ответов'

    def __str__(self):
        return self.name