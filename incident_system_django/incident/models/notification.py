from django.db import models
# Create your models here.


class NotificationType(models.Model):
    name = models.CharField(max_length=150,
                            verbose_name="Наименование типа оповещения")
    subject = models.CharField(max_length=150,
                               verbose_name="Шаблон текста темы сообщения")
    text = models.TextField(max_length=5000,
                            blank=True, null=True,
                            verbose_name="Шаблон текста сообщения")
    emails = models.TextField(max_length=6000,
                              blank=True,
                              null=True,
                              verbose_name="Шаблон переченя email адресов")
    emails_copy = models.TextField(max_length=6000,
                                   blank=True,
                                   null=True,
                                   verbose_name="Шаблон переченя email адресов в копии")
    emails_hidden = models.TextField(max_length=6000,
                                     blank=True,
                                     null=True,
                                     verbose_name="Шаблон переченя email адресов в скрытой копии")
    component = models.ManyToManyField('Component',
                                       verbose_name="Компонент",
                                       related_name='notification_types')
    send_plan_on_calendar = models.CharField(max_length=50,
                                             blank=True,
                                             null=True,
                                             verbose_name="Календарь отправок")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип рассылка'
        verbose_name_plural = 'Тип рассылки'


class Notification(models.Model):
    name = models.CharField(max_length=100,
                            blank=True,
                            null=True,
                            verbose_name="Назваение оповещения")
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name="Время отправки")
    subject = models.CharField(max_length=150,
                               verbose_name="Фактически отправленный текст темы сообщения")
    text = models.TextField(max_length=5000,
                            blank=True,
                            null=True,
                            verbose_name="Фактически отправленный текст сообщения")
    emails = models.TextField(max_length=6000,
                              verbose_name="Фактически отправленный перечень email адресов")
    emails_copy = models.TextField(max_length=6000,
                                   blank=True,
                                   null=True,
                                   verbose_name="Шаблон переченя email адресов в копии")
    emails_hidden = models.TextField(max_length=6000,
                                     blank=True,
                                     null=True,
                                     verbose_name="Шаблон переченя email адресов в скрытой копии")
    file = models.FileField(blank=True,
                            null=True,
                            upload_to='notifications/%Y/%m/%d',
                            verbose_name="Отправленный файл")
    file_name = models.CharField(blank=True,
                                 null=True,
                                 max_length=250,
                                 verbose_name="Имя файла")
    incident = models.ForeignKey('Incident',
                                 on_delete=models.CASCADE,
                                 verbose_name="Инцидент",
                                 related_name='notifications')
    notification_type = models.ForeignKey(NotificationType,
                                          on_delete=models.CASCADE,
                                          verbose_name="Тип рассылки",
                                          related_name='notifications')
    send_plan_on_calendar = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        verbose_name="Шаблон календаря отправок"
    )

    def __str__(self):
        return self.subject

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class NotificationQueue(models.Model):
    name = models.CharField(max_length=100,
                            blank=True,
                            null=True,
                            verbose_name="Назваение оповещения")
    subject = models.CharField(max_length=150,
                               verbose_name="Фактически отправленный текст темы сообщения")
    text = models.TextField(max_length=5000,
                            blank=True,
                            null=True,
                            verbose_name="Фактически отправленный текст сообщения")
    emails = models.TextField(max_length=6000,
                              verbose_name="Фактически отправленный перечень email адресов")
    emails_copy = models.TextField(max_length=6000,
                                   blank=True,
                                   null=True,
                                   verbose_name="Шаблон переченя email адресов в копии")
    emails_hidden = models.TextField(max_length=6000,
                                     blank=True,
                                     null=True,
                                     verbose_name="Шаблон переченя email адресов в скрытой копии")
    file = models.FileField(blank=True,
                            null=True,
                            upload_to='notifications/%Y/%m/%d',
                            verbose_name="Отправленный файл")
    file_name = models.CharField(blank=True,
                                 null=True,
                                 max_length=250,
                                 verbose_name="Имя файла")
    incident = models.ForeignKey(
        'Incident',
        on_delete=models.CASCADE,
        verbose_name="Инцидент",
        related_name='notifiction_queue'
    )
    notification_type = models.ForeignKey(
        NotificationType,
        on_delete=models.CASCADE,
        verbose_name="Тип рассылки",
        related_name='notification_queue'
    )
    send_at = models.DateTimeField(auto_now_add=False,
                                   verbose_name="Время и дата  отправки")

    def __str__(self):
        return self.subject

    class Meta:
        verbose_name = 'Очередь отправки оповещений'
        verbose_name_plural = 'Очередь отправки оповещений'
