from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Department(models.Model):
    name = models.CharField(max_length=50, default=None, verbose_name="Отдел")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'


class Employee(models.Model):
    name = models.CharField(max_length=30,
                            verbose_name="ФИО")
    department = models.ForeignKey(Department,
                                   on_delete=models.SET_NULL,
                                   null=True, verbose_name="Отдел")
    email = models.EmailField(blank=True)
    phone = models.CharField(blank=True,
                             max_length=20,
                             verbose_name="Телефон")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.ForeignKey(Department,
                                   on_delete=models.SET_NULL,
                                   blank=True,
                                   null=True, verbose_name="Отдел")
    phone = models.CharField(verbose_name="Телефоннный номер",
                             max_length=20,
                             blank=True)

    class Meta:
        verbose_name = 'Расширенный профиль'
        verbose_name_plural = 'Расширенные профили'

    def __str__(self):
        return f'{self.user.username}'

    def save(self, *args, **kwargs):
        super().save()


@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    # так как расширили модель  на базе с существующими пользователями требуется создать profile
    if created or not hasattr(instance, "profile"):
        Profile.objects.create(user=instance)
    instance.profile.save()
