from django.db import models
from django.core.exceptions import ValidationError
from django.contrib import admin
from django.contrib.auth.models import User
# Create your models here.
from .profile import Employee


def get_first_name(self):
    return f"{self.first_name} {self.last_name}"


User.add_to_class("__str__", get_first_name)


class Direction(models.Model):
    name = models.CharField(max_length=50,
                            help_text="Напровление",
                            verbose_name="Направление")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'


class Service(models.Model):
    name = models.CharField(max_length=50,
                            help_text="Название сервиса",
                            verbose_name="Сервис")
    direction = models.ForeignKey(Direction,
                                  on_delete=models.SET_NULL,
                                  null=True,
                                  blank=True,
                                  verbose_name="Направление")
    sla = models.DecimalField(max_digits=4,
                              decimal_places=2,
                              null=True,
                              blank=True,
                              verbose_name="SLA для сервиса")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сервис'
        verbose_name_plural = 'Сервисы'


class Influence(models.Model):
    name = models.CharField(max_length=50,
                            help_text="Влияние на бизнес",
                            verbose_name="Влияние на бизнес")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Влияние на бизнес'
        verbose_name_plural = 'Влияния на бизнес'


class InternalSupport(models.Model):
    name = models.CharField(max_length=50,
                            null=True,
                            blank=True,
                            help_text="Название команды",
                            verbose_name="Название команды")
    description = models.TextField(null=True,
                                   blank=True,
                                   verbose_name="Информация о команде")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Команда внутренней эксплуатации'
        verbose_name_plural = 'Команда внутренней эксплуатации'


class ExternalSupport(models.Model):
    name = models.CharField(max_length=50,
                            null=True,
                            blank=True,
                            help_text="Название команды",
                            verbose_name="Название команды")
    description = models.TextField(null=True,
                                   blank=True,
                                   verbose_name="Информация о команде")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Команда внешней эксплуатации'
        verbose_name_plural = 'Команда внешней эксплуатации'


class InternalContact(models.Model):
    name = models.CharField(max_length=50,
                            null=True,
                            blank=True,
                            help_text="Название контакта",
                            verbose_name="Название контакта")
    description = models.TextField(null=True,
                                   blank=True,
                                   verbose_name="Информация о контакте")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Внутренний контакт'
        verbose_name_plural = 'Внутренние контакты'


class ExternalContact(models.Model):
    name = models.CharField(max_length=50,
                            null=True,
                            blank=True,
                            help_text="Название контакта",
                            verbose_name="Название контакта")
    description = models.TextField(null=True,
                                   blank=True,
                                   verbose_name="Информация о контакте")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Внешний  контакт'
        verbose_name_plural = 'Внешние  контакты'


class Component(models.Model):
    RESPONSIBLE = (('IN', 'Внутреннее'),
                   ('EX', 'Внешнее'),
                   ('CL', 'Заказчик'))
    name = models.CharField(max_length=50,
                            help_text="Название системы",
                            verbose_name="Системы")
    owner = models.ForeignKey(Employee,
                              on_delete=models.SET_NULL,
                              null=True,
                              blank=True,
                              default=None,
                              verbose_name="Менеджер системы",
                              related_name='components')
    service = models.ManyToManyField(Service,
                                     blank=True,
                                     verbose_name="Сервис",
                                     related_name='components')
    internal_support = models.ForeignKey(InternalSupport,
                                         on_delete=models.SET_NULL,
                                         null=True,
                                         blank=True,
                                         default=None,
                                         verbose_name="Команда внутренней эксплуатации",
                                         related_name='components')
    external_support = models.ForeignKey(ExternalSupport,
                                         on_delete=models.SET_NULL,
                                         null=True, blank=True,
                                         default=None,
                                         verbose_name="Команда внешней эксплуатации",
                                         related_name='components')
    internal_contact = models.ForeignKey(InternalContact,
                                         on_delete=models.SET_NULL,
                                         null=True,
                                         blank=True,
                                         default=None,
                                         verbose_name="Внутренние контакты",
                                         related_name='components')
    external_contact = models.ForeignKey(ExternalContact,
                                         on_delete=models.SET_NULL,
                                         null=True,
                                         blank=True,
                                         default=None,
                                         verbose_name="Внешние контакты",
                                         related_name='components')
    responsible = models.CharField(max_length=2,
                                   default=None,
                                   choices=RESPONSIBLE,
                                   verbose_name="Обслуживание")
    influence = models.ForeignKey(Influence,
                                  on_delete=models.SET_NULL,
                                  null=True,
                                  verbose_name="Влияние на бизнес")
    sla = models.DecimalField(max_digits=4,
                              decimal_places=2,
                              null=True,
                              blank=True,
                              verbose_name="SLA для системы")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Система'
        verbose_name_plural = 'Системы'
        ordering = ['name']


class Category(models.Model):
    name = models.CharField(max_length=50,
                            help_text="Категория инцидента",
                            verbose_name="Категория инцидента")
    sla = models.BooleanField(default=False,
                              verbose_name="Категория инцидента участвует в расчёте SLA")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория инцидента'
        verbose_name_plural = 'Категории инцидентов'


class Status(models.Model):
    name = models.TextField(max_length=50,
                            verbose_name="Статус")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'


class Workflow(models.Model):
    name = models.TextField(max_length=150,
                            blank=True,
                            null=True,
                            verbose_name="Название процесса")
    label = models.TextField(max_length=150,
                             verbose_name="Пояснение")
    status = models.ForeignKey(Status,
                               on_delete=models.SET_NULL,
                               null=True,
                               verbose_name="Статус",
                               related_name='statuses')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Процесс'
        verbose_name_plural = 'Процессы'


class Affected(models.Model):
    name = models.CharField(max_length=30,
                            blank=True,
                            null=True,
                            verbose_name="Количество затрагиваемых сотрудников")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Количество затрагиваемых сотрудников'
        verbose_name_plural = 'Количество затрагиваемых сотрудников'


class ProblemStatus(models.Model):
    name = models.CharField(verbose_name="Статус проблемы",
                            max_length=30,
                            blank=True,
                            null=True
                            )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус проблемы'
        verbose_name_plural = 'Статусы проблем'


class Problem(models.Model):
    id = models.AutoField(primary_key=True,
                          verbose_name="Номер проблемы")
    status = models.ForeignKey(ProblemStatus,
                               verbose_name="Статус",
                               default=None,
                               null=True,
                               on_delete=models.CASCADE,
                               related_name='statuses')
    name = models.CharField(verbose_name="Наименование проблемы",
                            max_length=30,
                            blank=True,
                            null=True
                            )
    start_date = models.DateTimeField(verbose_name="Дата и время создания проблемы",
                                      null=True,
                                      blank=True)
    end_date = models.DateTimeField(verbose_name="Дата и время регшения проблемы",
                                    null=True,
                                    blank=True)
    component = models.ForeignKey(Component,
                                  verbose_name="Система",
                                  on_delete=models.CASCADE,
                                  null=True,
                                  related_name='components')
    description = models.TextField(verbose_name="Описание",
                                   blank=True)
    cause = models.TextField(verbose_name="Причина",
                             blank=True)
    prevent = models.TextField(verbose_name="Мероприятия по недопущению",
                               blank=True)
    monitoring = models.TextField(verbose_name="Мониторинг",
                                  blank=True)

    def __str__(self):
        return str(self.id)
    
    def incidents_count(self):
        return len(self.incidents)

    class Meta:
        verbose_name = 'Проблема'
        verbose_name_plural = 'Проблемы'


class Incident(models.Model):
    id = models.AutoField(primary_key=True,
                          verbose_name="Номер аварии")
    start_date = models.DateTimeField(verbose_name="Дата и время начала")
    end_date = models.DateTimeField(verbose_name="Дата и время окончания")
    author = models.ForeignKey(User,
                               verbose_name="Автор инцидента",
                               null=True,
                               blank=True,
                               on_delete=models.CASCADE)
    component = models.ForeignKey(Component,
                                  on_delete=models.CASCADE,
                                  null=True,
                                  verbose_name="Система",
                                  related_name='incidents')
    category = models.ForeignKey(Category,
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 verbose_name="Категория",
                                 related_name='category')
    url = models.URLField(max_length=150,
                          blank=True,
                          verbose_name="Ссылка на задачу")
    description = models.TextField(verbose_name="Описание")
    employee = models.ForeignKey(Employee,
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 verbose_name="Ответственный сотрудник",
                                 related_name='incidents')
    affected = models.ForeignKey(Affected,
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 verbose_name="Количество затрагиваемых сотрудников",
                                 related_name='incidents')
    cause = models.TextField(blank=True,
                             null=True,
                             verbose_name="Причины")
    prevention = models.TextField(blank=True,
                                  null=True,
                                  verbose_name="Проведенные мероприятия и рекомендации\
                                  по устранению причин")
    external_problem = models.BooleanField(verbose_name="Проблема на стороне смежных систем")
    problem = models.ForeignKey(Problem,
                                verbose_name="Проблема",
                                on_delete=models.CASCADE,
                                null=True,
                                blank=True,
                                related_name='incidents')

    def __str__(self):
        return str(self.id)

    @property
    @admin.display(
        ordering='',
        description='Продолжительность',
    )
    def duration(self):
        duration = self.end_date - self.start_date
        return round(duration.total_seconds() / 60)

    class Meta:
        verbose_name = 'Инцидент'
        verbose_name_plural = 'Инциденты'

    # def save(self, *args, **kwargs):
    #   if self.end_date >= self.start_date:
    #       super(Incident, self).save(*args, **kwargs)
    #   else:
    #       raise ValidationError('Дата окончания не может быть раньше даты начала.')

    def clean(self):
        if self.start_date is None:
            raise ValidationError('Введите дату начала')
        if self.end_date is None:
            raise ValidationError('Введите дату окончания')
        if self.end_date <= self.start_date:
            raise ValidationError('Дата окончания не может быть раньше даты начала.')


class Chronology(models.Model):
    date = models.DateTimeField(verbose_name="Дата и время")
    job = models.TextField(max_length=500,
                           verbose_name="Действие/Событие")
    employee = models.ForeignKey(Employee,
                                 on_delete=models.SET_NULL,
                                 null=True)
    description = models.TextField(max_length=500,
                                   blank=True,
                                   verbose_name="Описание")
    incident = models.ForeignKey(Incident,
                                 on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        return self.job

    class Meta:
        verbose_name = 'Хронология инцилента'
        verbose_name_plural = 'Хронологии инцидентов'
