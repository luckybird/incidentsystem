from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from rangefilter.filters import DateRangeFilter
from django.forms import TextInput, Textarea
from django.db import models
from datetime import datetime

# Register your models here.
from .models.incident import (Direction,
                              Service,
                              Incident,
                              Chronology,
                              Component,
                              Category,
                              Influence,
                              Status,
                              Affected,
                              InternalSupport,
                              ExternalSupport,
                              InternalContact,
                              ExternalContact,
                              Problem,
                              ProblemStatus)

from .models.profile import (Profile,
                             Department,
                             Employee)

from .models.notification import NotificationType, Notification, NotificationQueue
from .models.answer import Answer

admin.site.register(Service)
admin.site.register(Chronology)
admin.site.register(Component)
admin.site.register(Category)
admin.site.register(Influence)
admin.site.register(Department)
admin.site.register(Direction)
admin.site.register(Status)
admin.site.register(Affected)
admin.site.register(InternalSupport)
admin.site.register(ExternalSupport)
admin.site.register(InternalContact)
admin.site.register(ExternalContact)
admin.site.register(Answer)
admin.site.register(ProblemStatus)

class InlineOnlyRead(admin.TabularInline):
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class IncidentsChronologyInline(admin.TabularInline):
    model = Chronology
    extra = 0
    classes = ('collapse',)


class NotificationInline(InlineOnlyRead):
    model = Notification
    readonly_fields = ('created_at',)
    fields = ('id', 'created_at',   'subject', 'emails', 'notification_type', 'text')


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'subject', 'emails')
    readonly_fields = ('created_at',)
    # def has_delete_permission(self, request, obj=None):
    #    return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class IncidentInline(InlineOnlyRead):
    model = Incident
    fields = ('id', 'start_date', 'end_date', 'component')

@admin.register(Problem)
class ProblemAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'name',
                    'start_date',
                    'end_date',
                    'component')
    ordering = ['-start_date']
    search_fields = ['id', 'name']
    inlines = [IncidentInline]


@admin.register(Incident)
class IncidentAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'start_date',
                    'end_date',
                    'category',
                    'component',
                    'external_problem',
                    'duration',)
    list_filter = ['component',
                   'start_date',
                   'component__service',
                   'category',
                   'external_problem']
    list_filter = (('start_date', DateRangeFilter),
                   ('start_date'),
                   ('end_date', DateRangeFilter),
                   ('end_date'),
                   ('component'),
                   ('category'),
                   ('external_problem'))
    # fields = ('start_date',
    #           'end_date',
    #           'category',
    #           'component',
    #           'external_problem',
    #           'problem'
    #           )
    autocomplete_fields = ('problem', )
    search_fields = ['id', 'component__name']
    ordering = ['-start_date']
    inlines = [IncidentsChronologyInline, NotificationInline]
    readonly_fields = ('author',)

    def get_rangefilter_created_at_default(self, request):
        return (datetime.date.today, datetime.date.today)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('category', 'component')

    def save_model(self, request, obj, form, change):
        if not obj.author:
            obj.author = request.user
        super().save_model(request, obj, form, change)


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('name', 'department')


@admin.register(NotificationType)
class NotificationTypeAdmin(admin.ModelAdmin):
    list_filter = ['component']
    exclude = ('sent_to_time',)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '150'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 20, 'cols': 150})},
    }


@admin.register(NotificationQueue)
class NotificationQueueAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'send_at',
                    'name',
                    'subject',
                    'incident'
                    )
    list_filter = (('send_at', DateRangeFilter),
                   ('send_at')
                   )
    ordering = ['-send_at']

    def get_rangefilter_created_at_default(self, request):
        return (datetime.date.today, datetime.date.today)


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "расширенный профиль"


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = [ProfileInline]


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
