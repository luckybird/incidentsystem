# from django.db.models import fields
from rest_framework import serializers
from .models.incident import Component, Incident, Category


class ComponentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Component
        fields = (
            'id', 'name',
        )

        datatables_always_serialize = ('id',)


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Category
        fields = (
            'id', 'name',
        )

        datatables_always_serialize = ('id',)


class IncidentSerializer(serializers.ModelSerializer):

    component_name = serializers.ReadOnlyField(source='component.name')
    component = ComponentSerializer()
    category_name = serializers.ReadOnlyField(source='category.name')
    category = CategorySerializer()
    #   duration_time = serializers.ReadOnlyField(source='duration')

    class Meta:
        model = Incident
        fields = ('id',
                  'start_date',
                  'end_date',
                  'component_name',
                  'component',
                  'category_name',
                  'category',
                  'external_problem',
                  'description',
                  'cause',
                  'duration')
