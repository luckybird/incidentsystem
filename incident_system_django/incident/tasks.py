from celery import shared_task
from celery.utils.log import get_task_logger

from django.core.mail import EmailMultiAlternatives
from smtplib import SMTPException
from django.conf import settings
from .models.notification import Notification, NotificationQueue

from ast import literal_eval
import mimetypes
import datetime

logger = get_task_logger(__name__)


@shared_task(name="sample_task")
def sample_task():
    logger.info("The sample task just ran.")


@shared_task
def send_mail_task(emails_subject,
                   emails,
                   emails_copy,
                   emails_hidden,
                   text_content,
                   notification_id):
    # logger.info(emails_subject)

    email = EmailMultiAlternatives(
        emails_subject,
        text_content,
        settings.FROM_EMAIL,
        emails,
        bcc=emails_hidden,
        cc=emails_copy,
    )
    email.content_subtype = "html"
    notification = Notification.objects.get(pk=notification_id)
    filename = str(notification.file)
    if filename:
        with open('.' + settings.MEDIA_URL + filename, "rb") as file:
            email.attach(str(notification.file_name),
                         file.read(),
                         mimetypes.guess_type(file.name)[0])
            email.attach_alternative(text_content, "text/html")
            try:
                email.send()
            except SMTPException as e:
                logger.error(e)
    else:
        try:
            email.send()
        except SMTPException as e:
            logger.error(e)


@shared_task(name="send_scheduled_mail_task")
def send_scheduled_mail_task():
    logger.info("Sending email")
    print("Sending email")
    now_date = datetime.datetime.now()
    scheduled_mail = NotificationQueue.objects.filter(send_at__lte=now_date)
    for mail in scheduled_mail:
        to = literal_eval(mail.emails)
        bcc = literal_eval(mail.emails_hidden)
        cc = literal_eval(mail.emails_copy)
        email = EmailMultiAlternatives(
            mail.subject,
            mail.text,
            settings.FROM_EMAIL,
            to,
            bcc=bcc,
            cc=cc,
        )
        email.content_subtype = "html"
        filename = mail.file
        if filename:
            with open('.' + settings.MEDIA_URL + filename, "rb") as file:
                email.attach(str(mail.file_name),
                             file.read(),
                             mimetypes.guess_type(file.name)[0])
                email.attach_alternative(mail.text, "text/html")
                try:
                    email.send()
                    NotificationQueue.objects.filter(pk=mail.pk).delete()
                except SMTPException as e:
                    logger.error(e)
        else:
            try:
                email.send()
                NotificationQueue.objects.filter(pk=mail.pk).delete()
            except SMTPException as e:
                logger.error(e)


#    def create_send_task(self, incident):
#         if self.plan_type == 'NOW':
#             return [datetime.date]
#         elif self.plan_type == 'ONDATE':
#             return [self.on_date]
#         elif self.plan_type == 'CALENDAR':
#             res = []
#             for x in self.calendar.split(','):
#                 dt, tm = x.split('(')
#                 dt = int(dt)
#                 tm = tm.split(')')[0]
#                 end_date = incident.start_date - timedelta(days=dt * -1)
#                 end_date_txt = end_date.strftime('%d.%m.%Y')

#                 res.append(datetime.strptime(f'{end_date_txt} {tm}', '%d.%m.%Y %H:%M'))
#             return res
